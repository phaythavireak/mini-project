package com.virak.demo.services;

import com.virak.demo.models.Book;

import java.util.List;

public interface BooksService {
    List<Book> getAll();
    Book findOne(Integer id);
    boolean Uppate(Book book);
    boolean remove(Integer id);
    Integer count();
    boolean create(Book book);
}
