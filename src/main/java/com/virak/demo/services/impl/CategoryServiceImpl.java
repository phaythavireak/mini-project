package com.virak.demo.services.impl;

import com.virak.demo.models.Category;
import com.virak.demo.repositories.CategoryRepository;
import com.virak.demo.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CategoryServiceImpl implements CategoryService {
    private CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> getAll() {
        return this.categoryRepository.getAll();
    }

    @Override
    public Integer count() {
        return this.categoryRepository.count();
    }

    @Override
    public boolean create(Category category) {
        return this.categoryRepository.create(category);
    }

    @Override
    public Category find(Integer id) {
        return this.categoryRepository.find(id);
    }

    @Override
    public boolean remove(Integer id) {
        return this.categoryRepository.remove(id);
    }

    @Override
    public boolean update(Category category) {
        return this.categoryRepository.update(category);
    }
}
