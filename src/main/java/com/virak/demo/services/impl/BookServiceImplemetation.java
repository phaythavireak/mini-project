package com.virak.demo.services.impl;

import com.virak.demo.models.Book;
import com.virak.demo.repositories.BookRepositories;
import com.virak.demo.services.BooksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImplemetation implements BooksService {
    private BookRepositories bookRepositories;
    @Autowired
    public BookServiceImplemetation(BookRepositories bookRepositories) {
        this.bookRepositories = bookRepositories;
    }
    @Override
    public List<Book> getAll() {
        return this.bookRepositories.getAll();
    }

    @Override
    public Book findOne(Integer id) {
        return this.bookRepositories.findOne(id);
    }

    @Override
    public boolean Uppate(Book book) {
        return this.bookRepositories.Update(book);
    }

    @Override
    public boolean remove(Integer id) {
        return this.bookRepositories.remove(id);
    }

    @Override
    public Integer count() {
        return this.bookRepositories.count();
    }

    @Override
    public boolean create(Book book) {
        return this.bookRepositories.create(book);
    }
}
