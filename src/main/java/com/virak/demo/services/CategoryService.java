package com.virak.demo.services;

import com.github.javafaker.Cat;
import com.virak.demo.models.Category;
import org.springframework.stereotype.Service;

import java.util.List;
public interface CategoryService {
    List<Category> getAll();
    Integer count();
    boolean create(Category category);
    Category find(Integer id);
    boolean remove(Integer id);
    boolean update(Category category);
}
