package com.virak.demo.repositories;

import com.virak.demo.models.Book;
import com.virak.demo.repositories.provider.BookProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepositories {

    @SelectProvider(type = BookProvider.class, method = "getAllProvider")
    @Results({
            @Result(column = "id", property = "id"),
            @Result(column = "cate_id", property = "category.id"),
            @Result(column = "name", property = "category.name")
    })
    List<Book> getAll();

    @InsertProvider(type = BookProvider.class, method = "create")
    boolean create(Book book);


    @Select("select * from books b INNER JOIN category c ON b.cate_id = c.id where b.id=#{id}")
    @Results({
            @Result(column = "id", property = "id"),
            @Result(column = "cate_id", property = "category.id"),
            @Result(column = "name", property = "category.name")
    })
    Book findOne(@Param("id") Integer id);

    @Update("update books set title=#{title}, author=#{author}, publisher=#{publisher}, thumnail=#{thumnail}, cate_id=#{category.id} where id=#{id}")
    boolean Update(Book book);

    @Delete("delete from books where id=#{id}")
     boolean remove(Integer id);

    @Select("select count(*) from books")
     Integer count();


}
