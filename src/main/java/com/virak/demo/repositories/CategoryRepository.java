package com.virak.demo.repositories;

import com.virak.demo.models.Category;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface CategoryRepository {

    @Select("select * from category order by id")
    List<Category> getAll();

    @Insert("insert into category(name) values(#{name})")
    boolean create(Category category);
    @Select("select count(*) from category")
    Integer count();

    @Select("select * from category where id=#{id}")
    Category find(Integer id);

    @Delete("delete from category where id=#{id}")
    boolean remove(Integer id);
    @Update("update category set name=#{name} where id=#{id}")
    boolean update(Category category);
}
