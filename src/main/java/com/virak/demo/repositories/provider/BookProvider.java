package com.virak.demo.repositories.provider;

import com.virak.demo.models.Book;
import org.apache.ibatis.jdbc.SQL;
public class BookProvider {


    public String getAllProvider(){
        return new SQL(){{
            SELECT("*");
            FROM("books b");
            INNER_JOIN("category c ON b.cate_id = c.id");
            ORDER_BY("b.id desc");
        }}.toString();
    }


    public String create(Book book){
        return new SQL(){{
            INSERT_INTO("books");
            VALUES("title", "#{title}");
            VALUES("author", "#{author}");
            VALUES("publisher", "#{publisher}");
            VALUES("thumnail", "#{thumnail}");
            VALUES("cate_id" ,"#{category.id}");

        }}.toString();
    }
}
