package com.virak.demo.models;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class Book {

//    @Positive
    public int id;
    @NotNull
    @Size(min = 5,max = 100,message = "can't Null and be greater then 5")
    public String title;
    @NotNull
    @Size(min = 5,max = 100,message = "can't Null and be greater then 5")
    public String author;
    @NotNull
    @Size(min = 5,max = 100,message = "can't Null and be greater then 5")
    public String publisher;
    public String Thumnail;
    private Category category = new Category();
    public Book() {

    }

    public Book( int id, @NotNull @Size(min = 5, max = 50, message = "can't Null and be greater then 5")  String title, String author, String publisher, String thumnail, Category category) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        Thumnail = thumnail;
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getThumnail() {
        return Thumnail;
    }

    public void setThumnail(String thumnail) {
        Thumnail = thumnail;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", publisher='" + publisher + '\'' +
                ", Thumnail='" + Thumnail + '\'' +
                ", categires=" + category +
                '}';
    }
}
