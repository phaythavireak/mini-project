package com.virak.demo.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class Category {
    private Integer id;
    @NotNull
    @Size(min = 5,max = 100,message = "can't Null and be greater then 5")
    private String name;
    List<Book> book;
    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBook(List<Book> book) {
        this.book = book;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Book> getBook() {
        return book;
    }

    public Category() {

    }

    public Category(Integer id, String name, List<Book> book) {
        this.id = id;
        this.name = name;
        this.book = book;
    }


    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", book=" + book +
                '}';
    }
}
