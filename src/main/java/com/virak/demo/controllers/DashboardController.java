package com.virak.demo.controllers;

import com.virak.demo.services.BooksService;
import com.virak.demo.services.CategoryService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DashboardController {
    BooksService booksService;
    CategoryService categoryService;


    public DashboardController(BooksService booksService, CategoryService categoryService) {
        this.booksService = booksService;
        this.categoryService = categoryService;
    }

    @RequestMapping("/")
    public String index(Model model)
    {
        model.addAttribute("counts",this.categoryService.count());
        model.addAttribute("count", this.booksService.count());
        return "dashboard/dashboard";
    }
}
