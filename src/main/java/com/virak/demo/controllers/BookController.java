package com.virak.demo.controllers;

import com.virak.demo.models.Book;
import com.virak.demo.models.Category;
import com.virak.demo.services.BooksService;
import com.virak.demo.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Controller
public class BookController {

    private BooksService booksService;
    @Autowired
    private CategoryService categoryService;

    @Autowired
    public BookController(BooksService booksService) {
        this.booksService = booksService;
    }

    // @GetMapping("/index")
    @RequestMapping(method = RequestMethod.GET, value = {"/index", "home"})
    public String index(ModelMap model) {
        List<Book> bookList = this.booksService.getAll();
        model.addAttribute("books", bookList);
        return "book/index";
    }


    @GetMapping("/view/{id}")
    public String viewDetail(@PathVariable("id") Integer id, ModelMap model) {
        Book book = this.booksService.findOne(id);
        model.addAttribute("book", book);
        return "book/view-detail";
    }

    @GetMapping("/update/{id}")
    public String showUpdateForm(@PathVariable Integer id, ModelMap model) {
        Book book = this.booksService.findOne(id);
        model.addAttribute("isnew", false);
        model.addAttribute("book", book);
        List<Category> categories = this.categoryService.getAll();
        model.addAttribute("categories", categories);
        return "book/create-book";
    }

    @PostMapping("update/submit")
    public String updateSubmit(@ModelAttribute Book book, MultipartFile file) {
        System.out.println(book);

        File path = new File("/Users/sbc/Documents/Assignment/pp6th/");

        if (!path.exists())
            path.mkdirs();

        String filename = file.getOriginalFilename();
        String extension = filename.substring(filename.lastIndexOf('.') + 1);

        System.out.println(filename);
        System.out.println(extension);

        filename = UUID.randomUUID() + "." + extension;
        System.out.println(filename);
        try {
            Files.copy(file.getInputStream(), Paths.get("/Users/sbc/Documents/Assignment/pp6th/", filename));
        } catch (IOException e) {
            e.printStackTrace();
        }
//        if(file.isEmpty())
//        {
//            book.setThumnail("/images-pp/"+filename);
//        }

        if (file.isEmpty()) {
            String tem = booksService.findOne(book.id).getThumnail();
            book.setThumnail(tem);
        } else {
            book.setThumnail(filename);
        }

//        book.setThumnail(filename);

        this.booksService.Uppate(book);
        return "redirect:/index";

    }

    @GetMapping("remove/{id}")
    public String remove(@PathVariable Integer id) {
        this.booksService.remove(id);

        return "redirect:/";
    }


    @GetMapping("/count")
    @ResponseBody
    public Map<String, Object> count() {
        Map<String, Object> response = new HashMap<>();

        response.put("record_count", this.booksService.count());
        response.put("status", true);

        return response;
    }

    @ResponseBody
    public String count(Model model) {
        model.addAttribute("count", this.booksService.count());
        return "dashboard/dashboard";


    }


    @GetMapping("/create")
    public String create(Model model) {
        model.addAttribute("book", new Book());
        model.addAttribute("isnew", true);
        List<Category> categories = this.categoryService.getAll();
        model.addAttribute("categories", categories);
        System.out.println(categories);
        return "book/create-book";
    }

    @PostMapping("/create/submit")
    public String createSubmit(@Valid Book book,
                               BindingResult bindingResult, MultipartFile file, Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("isnew", true);
            List<Category> categories = this.categoryService.getAll();
            model.addAttribute("categories", categories);
            return "book/create-book";
        }

//        if (file == null)return null;
//
//        File path = new File("/pp6th");
//        if (!path.exists())
//            path.mkdirs();
        if (file == null) return null;
        File path = new File("/Users/sbc/Documents/Assignment/pp6th/");
        if (!path.exists())
            path.mkdirs();

        String filename = file.getOriginalFilename();
        String extension = filename.substring(filename.lastIndexOf('.') + 1);

        System.out.println(filename);
        System.out.println(extension);

        filename = UUID.randomUUID() + "." + extension;
        System.out.println(filename);
        try {
            Files.copy(file.getInputStream(), Paths.get("/Users/sbc/Documents/Assignment/pp6th/", filename));
        } catch (IOException e) {
            e.printStackTrace();
        }
        book.setThumnail("/images-pp/" + filename);
        book.setThumnail(filename);


        this.booksService.create(book);
        return "redirect:/home";

    }




}





















