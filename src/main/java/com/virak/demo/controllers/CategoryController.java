package com.virak.demo.controllers;

import com.virak.demo.models.Book;
import com.virak.demo.models.Category;
import com.virak.demo.services.CategoryService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class CategoryController {
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    private CategoryService categoryService;
    @RequestMapping("/category")
    public String home(Model model)
    {

        List<Category> categorys= this.categoryService.getAll();
        model.addAttribute("cate",categorys);
        return "category/home";
    }
    @GetMapping("/category/create")
    public String create(Model model)
    {
        model.addAttribute("change", true);
        model.addAttribute("categories",new Category());
        return "category/create";
    }
    @PostMapping("/category/submit")
    public String submit(Category category,BindingResult bindingResult)
    {
        this.categoryService.create(category);
        return "redirect:/category";
    }
    @GetMapping("category/view/{id}")
    public String view(@PathVariable Integer id,Model model) {
        Category category1 = this.categoryService.find(id);
        model.addAttribute("category", category1);
        return "category/detail";
    }
    @GetMapping("category/remove/{id}")
    public String remove(@PathVariable Integer id) {
        this.categoryService.remove(id);

        return "redirect:/category";
    }
    @GetMapping("category/update/{id}")
    public String updates(@PathVariable Integer id,Model model) {
        Category category2 = this.categoryService.find(id);
        model.addAttribute("category", category2);
        return "category/update";
    }
    @PostMapping("/category/update/submit")
    public String update(@ModelAttribute Category category)
    {
        this.categoryService.update(category);
        System.out.println(category);
        return "redirect:/category";
    }
}
