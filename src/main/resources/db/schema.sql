create table category (
  id serial primary key ,
  name VARCHAR
);


create table books (
  id serial primary key ,
  title varchar,
  author varchar ,
  publisher varchar,
  thumnail varchar,
  cate_id int references category(id)
);